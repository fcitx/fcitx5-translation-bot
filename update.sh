#!/bin/bash
set -e
cd "$(dirname $0)"
basedir=$PWD

if [ -d .git ]; then
    git pull
fi

source ./functions.sh

mkdir -p repos || exit 1

cd repos
mkdir -p .tx || exit 1
txfile=$PWD/.tx/config

cat > "${txfile}" <<EOF
[main]
host = https://www.transifex.com
EOF

while IFS='' read -r project_line || [[ -n "$project_line" ]]; do
    read -r project projectdir <<<${project_line}
    pushd .
    if [ -d "$project" ]; then
        cd "$project"
        git fetch --all
        branch=`git symbolic-ref --short HEAD`
        git checkout $branch
        git reset --hard origin/$branch
        git clean -dfx
    else
        git clone "ssh://git@github.com/fcitx/${project}"
        cd "$project"
    fi

    echo $project $projectdir
    if [ -n "$projectdir" ]; then
        cd "$projectdir"
    fi
    potfile=$(find -name '*.pot' | head -n 1)
    potfile=$(basename $potfile)
    potfile=${potfile/.pot/}

    echo "[fcitx.$potfile]" >> "${txfile}"
    if [ -n "$projectdir" ]; then
        echo "file_filter = $project/$projectdir/po/<lang>.po" >> "${txfile}"
        echo "source_file = $project/$projectdir/po/$potfile.pot" >> "${txfile}"
    else
        echo "file_filter = $project/po/<lang>.po" >> "${txfile}"
        echo "source_file = $project/po/$potfile.pot" >> "${txfile}"
    fi
    echo "source_lang = en" >> "${txfile}"

    bash Messages.sh
    popd
done < "$basedir/projects"

tx push -s --no-interactive
tx pull -f --mode=developer -a --minimum-perc=1

while IFS='' read -r project_line || [[ -n "$project_line" ]]; do
    read -r project projectdir <<<${project_line}
    pushd .
    cd $project
    pushd .
    if [ -n "$projectdir" ]; then
        cd "$projectdir"
    fi
    bash Messages.sh

    popd
    git add .
    if check_change; then
            git commit --author="Fcitx Transifex Bot <fcitx-dev@googlegroups.com>" -m "[trans] Update Translation"
            branch=`git symbolic-ref --short HEAD`
            git push origin $branch
    else
            echo "nothing special, dont update"
            git reset --hard HEAD
    fi
    popd
done < "$basedir/projects"

