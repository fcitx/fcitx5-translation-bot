
check_change() {
    if git diff --cached --numstat HEAD | grep -vP '^1\t1\t'; then
        potfile=`git diff --cached --numstat HEAD | grep pot$ | cut -f 3`
        flag=1
        if [ -f "$potfile" ]; then
            if git diff --cached "$potfile" | python3 "$basedir/check.py"; then
                flag=0;
            else
                git reset "$potfile"
                git checkout "$potfile"
            fi
        fi
        for po in `git diff --cached --numstat HEAD | grep po$ | cut -f 3`; do
            if git diff --cached "$po" | python3 "$basedir/check.py"; then
                flag=0
            else
                git reset "$po"
                git checkout "$po"
            fi
        done

        return $flag
    else
        return 1
    fi
}


