#!/usr/bin/python3

import sys
import fileinput

blacklist=["PO-", "POT-", "Last-Translator",
"Project-Id-Version:",
"Report-Msgid-Bugs-To:",
"Language-Team:",
"Language:",
"MIME-Version:",
"Content-Type:",
"Content-Transfer-Encoding:"]


add = dict()
remove = dict()

for line in fileinput.input(openhook=fileinput.hook_encoded("utf-8")):
	if line.endswith("\n"):
		line = line[:-2]
	if not line.startswith("+") and not line.startswith("-"):
		continue
	if len(line) < 2:
		continue
	if line[1] == '#':
		continue
	if line.startswith("+++") or line.startswith("---"):
		continue
	if len(line) > 3:
		flag=False
		for bl in blacklist:
			if bl in line[2:]:
				flag=True
				break
		if flag:
			continue
	if line.startswith("+"):
		if line[1:] not in add:
			add[line[1:]] = 1
		else:
			add[line[1:]] += 1
	else:
		if line[1:] not in remove:
			remove[line[1:]] = 1
		else:
			remove[line[1:]] += 1

if len(add) != len(remove):
	sys.exit(0)

k1 = set(add.keys())
k2 = set(remove.keys())

#print (k1)
#print (k2)

if len(k1.intersection(k2)) != len(k1):
	sys.exit(0)

for (s, c) in add.items():
	if s not in remove:
		sys.exit(0)
	elif c != remove[s]:
		sys.exit(0)

sys.exit(1)

